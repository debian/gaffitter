# $Id: Makefile 151 2008-06-04 01:35:38Z daaugusto $
# Copyright (C) 2005-2008 Douglas A. Augusto (daaugusto@gmail.com)

all:
	cd src; $(MAKE)
clean:
	cd src; $(MAKE) clean
install:
	cd src; $(MAKE) install
uninstall:
	cd src; $(MAKE) uninstall
