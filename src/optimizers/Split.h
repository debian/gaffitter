// ---------------------------------------------------------------------
// $Id: Split.h 230 2008-08-14 01:45:04Z daaugusto $
//
//   Split.h (created on Fri Aug  4 12:59:27 BRT 2006)
// 
//   Genetic Algorithm File Fitter (gaffitter)
//
//   Copyright (C) 2005-2008 Douglas A. Augusto
// 
// This file is part of gaffitter.
// 
// gaffitter is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
// 
// gaffitter is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gaffitter; if not, see <http://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------

#ifndef Split_hh
#define Split_hh

#include "../Optimizer.h"

// ---------------------------------------------------------------------
/**
 * Split an input into pieces
 */
class Split: public Optimizer {
public:
   /**
    * Calls the base constructor and prints some information if needed.
    */
   Split( Input& i ): Optimizer( i ) {}

   ~Split() { delete m_solution; }
   /**
    * Just splits the input (in original order) according to target size,
    * i.e., select file by file until fit the specified volume.
    */
   void Evolve();

protected:
   /** 
    * Writes some information (like algorithm name and parameters) in
    * ostream object (usually cout).
    */
   std::ostream& Write( std::ostream& ) const; 
};

// ---------------------------------------------------------------------

#endif
