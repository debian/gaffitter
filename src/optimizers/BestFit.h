// ---------------------------------------------------------------------
// $Id: BestFit.h 230 2008-08-14 01:45:04Z daaugusto $
//
//   BestFit.h (created on Sat Nov 19 18:00:35 BRT 2005)
// 
//   Genetic Algorithm File Fitter (gaffitter)
//
//   Copyright (C) 2005-2008 Douglas A. Augusto
// 
// This file is part of gaffitter.
// 
// gaffitter is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
// 
// gaffitter is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gaffitter; if not, see <http://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------

#ifndef BestFit_hh
#define BestFit_hh

#include "../Optimizer.h"
#include <algorithm>

// ---------------------------------------------------------------------
/**
 * Optimization via the Best Fit search.
 */
class BestFit: public Optimizer {
public:
   /**
    * Calls the base constructor and print some information if needed.
    */
   BestFit( Input& i ): Optimizer( i ) 
   {
      // sorts files by size (reverse order)
      std::sort( m_files.begin(), m_files.end(), SizeName::CmpSizeRev );
   }

   ~BestFit() { delete m_solution; }

   /**
    * Create a reasonable solution via "Best Fit Search", i.e., select
    * files from bigger to smaller until no more space left (less than
    * target size). This algorithm usually produces locally optimal
    * solutions, however, it is very fast!
    */
   void Evolve();

protected:

   /** 
    * Writes some information (like algorithm name and parameters) in
    * ostream object (usually cout).
    */
   std::ostream& Write( std::ostream& ) const; 
};

// ---------------------------------------------------------------------

#endif
