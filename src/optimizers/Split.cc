// ---------------------------------------------------------------------------
// $Id: Split.cc 227 2008-08-13 20:51:32Z daaugusto $
//
//   Split.cc (created on Fri Aug  4 12:59:27 BRT 2006)
// 
//   Genetic Algorithm File Fitter (gaffitter)
//
//   Copyright (C) 2005-2008 Douglas A. Augusto
// 
// This file is part of gaffitter.
// 
// gaffitter is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the
// Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
// 
// gaffitter is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with gaffitter; if not, see <http://www.gnu.org/licenses/>.
//
// ---------------------------------------------------------------------------

#include "Split.h"

#include <vector>

using namespace std;

// ---------------------------------------------------------------------------
void
Split::Evolve()
{
   if( m_params.m_verbose ) cout << *this;

   m_solution = new BinSet();

   m_solution->AddBin( m_params.m_target );

   int bin = 0;
   for( unsigned i = 0; i < m_files.size(); ++i ) 
   { 
      if( (*m_solution)[bin].Free() >= m_files[i]->Size() )
      {
         (*m_solution)[bin].AddItem( m_files[i] );
      }
      else 
      {
         m_solution->AddBin( m_params.m_target ).AddItem( m_files[i] ); ++bin;
      }
   }
}

// ---------------------------------------------------------------------------
ostream& 
Split::Write( ostream& s ) const 
{ 
   s << endl;
   s << "> -----------------------------------" << endl;
   s << "> Split \"search\"                 "     << endl;
   s << "> -----------------------------------" << endl;

   Optimizer::Write( s );

   s << endl << flush;

   return s;
}

// ---------------------------------------------------------------------------
